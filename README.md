# Description de mon projet Portfolio

Dans le cadre de ce projet, j'ai réalisé un portfolio afin de présenter mon travail.
Jai commencé par réaliser les wiframes sur [Figma](https://www.figma.com/file/in81v8HFE7YYZm1HP9dqUy/Portfolio?node-id=0%3A1&t=e5VdyADAkucPnWpG-1) 


Le projet se compose de cinq parties: Home, About, Skills, Projects et Contact.
L'ensemble du site a été pensé en responsive à l'aide de bootstraps et des media queries.
Il a une barre de navigation latéral comprise dans un menu toggle avec des ancres pour naviguer au sein des différentes sections.

Pour la section Home, j'ai utlisé le CSS et les span pour styliser mon titre h1, et j'ai ajouté des lignes animées à l'aide de @keyframes.

Pour la section About, j'ai fait une petite présentation que j'ai stylisé avec une image en background et une animation pulse en hover sur les boutons.

Pour la section Skills, j'ai organisé mes compétences par articles dans deux colonnes juxtaposées en format ordinateur et supperposées en format mobile.

Pour la section Projects, j'ai organisé mes projets en articles, pour les personnaliser j'ai appliqué une rotation sur les titres pour les placer en latéral et j'ai animé les boutons. J'ai également ajouté un carroussel animé en rotation perpetuelle pour présenter quelques productions graphiques.

Pour la section Contact, je l'ai stylisé avec une image en background et une animation pulse en hover sur les boutons.


## Aperçu des wireframes

![<alt>](</images/Wireframe1.webp>)
![<alt>](</images/Wireframe2.webp>)


## Consignes du projet

Faire votre portfolio pour le site simplonlyon.fr dans lequel devront figurés :

- Une petite présentation
- La liste de vos projets
- Un contact
- Le lien vers votre gitlab
Eventuellement vos préférences en terme de langages/techno

Le site devra être responsive et valide W3C. Il est fortement conseillé d'utiliser la grid et les components bootstraps.

## Les liens concernant le projet Portfolio

- [ ] [Figma](https://www.figma.com/file/in81v8HFE7YYZm1HP9dqUy/Portfolio?node-id=0%3A1&t=e5VdyADAkucPnWpG-1) 
- [ ] [Gitlab](https://gitlab.com/Laure_G)
- [ ] [Netlify](https://portfolio-laure-glaizal.netlify.app/)

## Les outils utilisés

HTML pour la structure, CSS pour le style, bootstratp, les keyframes pour les animations, Figma pour réaliser les wireframes, Visual Studio Code et Gitlab.

## Test et Mise en ligne

Le HML/ CSS de ce projet a été testé avec W3C puis a été mis en ligne en utilisant Netlify, puis le lien Netlify a été ajouté à mon profil sur simplonlyon.fr


## Remerciements

Mes remerciements à [Ivan Liu Hu](https://unsplash.com/fr/@ivanliuhu) qui partage ses photographies libres de droits sur Unsplash, grâce à qui j'ai réalisé le background de ma section Accueil.

## Statut du projet

Ce projet pourra être amené à évoluer dans le temps, au fur et à mesure de mon parcours et de mes projets.
